### 1 引入swiper-dynamic-paginaton组件

```js
import sdp from '@/components/swiper-dynamic-pagination/swiper-dynamic-pagination.vue';
```

### 2 使用

```js
<!-- 指示点 --> 
<sdp :resdata="data" :currentIndex="swiperIndex" class="my-sdp"></sdp>
```

### 3 参数

可以看组件,就几个